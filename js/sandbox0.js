// Un commentaire sur une ligne
/*
message = "bienvenu dans j-s";
alert(message);
	*/


// Les tableaux de données
users = [ "Jhon", "Olivia", "Jack"];
message = "Bienvenue ds js " + users[1];
console.log(message); 


// les tableau peuvent ce composer de tous types de données
account = [ 12345, "Olivia", "Jones", true ];
console.log("N° d'employé: " + account[0] + " [" + typeof(account[0]) +"]");
console.log("Prénom : " + account[1] + " [" + typeof(account[1]) +"]");
console.log("Nom : " + account[2] + " [" + typeof(account[2]) +"]");
console.log("Statut :" + account[3] + " [" + typeof(account[3]) +"]");

// La propriété .lenght des tableaux
console.log("Longeur tableau : " + account.length);

// les fonctions js
function showDryMessage() {
	console.log (" dont repeat yourself");
}
showDryMessage();
console.log("Another line");
showDryMessage();


// la varible let
let season = "spring";
console.log("saison: " +season);
season="summer";
console.log("saison: " +season);
console.log("************************************************************");
// let season = autre chose ne marche pas 

// la variable const ne peut etre declarée qu une seule fois
const pi = 3.1416;

function cercleArea(radius){
	let area = pi * (radius **2);
	console.log("Aire: " + area);
}
 cercleArea(5);
 console.log("***********************************************************");

 //portée locale et globale des variables

 function showFruit(){
	let fruit = "lemon";
	console.log("fruit : " + fruit);
 }
 let fruit = "pomme";
 console.log("fruit : " + fruit);
 showFruit();
 console.log("***********************************************************");

 // passage de tableau au appel fonction

 function showCarData(car) {
	console.log("marque: "+car[0]);
	console.log("modèle: "+car[1]);
	console.log("carburant: "+car[2]);
	console.log("disponibilité: "+car[3]);
	console.log("long tableau"+car.length);
}
let car = ["toyota","yaris","hybride",true];
showCarData(car);
car = ["vw","polo","essence",true];
showCarData(car);
console.log("***********************************************************");

// utilisation de fct avec  valeur retournée---------------------------

const tva = 21;

function getTva(prix){
	let tvaInprix = prix + (prix/100*tva);
	return(tvaInprix);
}
function space(){
	console.log("***********************************************************");	
}
 let prixTotal = getTva(100);
 console.log(" prix ttc = " + prixTotal);
space();

// instruction if , true, else------------------------------------

if (2*3==6) {
	console.log("2*3=6");
}
space();
let active = true;
if (active){
	console.log("active est true");
}
let result = 15
if (result>100){
	console.log(result+" est > 100");
}else{
	console.log(result+" < ou = a 100");
}
console.log("***********************************************************");
let stock = 20 ; 
if (stock>50) {
	console.log(stock+" > 50");
	}
else if (stock >5) {
	console.log(stock+" > 5");
}
else {
	console.log(stock+" est trop petit");
}
space();
// operateur logique /*

let vegetable1= "carotte";
let vegetable2= "salade";
/*
if ((vegetable1=="carotte")&& (vegetable2== "salade")){
	console.log("les 2 cond ok");
	}
else{
	console.log("les 2 cond non respectée")
}	
*/
/*
if ((vegetable1=="carottes") || (vegetable2== "salade")){
	console.log("une ou l autre cond ok");
}
else {
	console.log("les 2 cond non respectée");
}	
*/
// switch case------------------------------------------------------------------------

 let color = "vert";
 switch (color) {
	case 'rouge':
		console.log("rouge");
		break;
	case 'vert':
		console.log("vert");
		break;
	default:
		console.log("csoisir une couleur");
 }


let item = "oiseau";

switch (item) {
	case "chat":
	case"chient":
	case"oiseau":
		console.log("c est un animal");
		break
	default	:
		console.log("choisir un animal");
}
space();

// boucle for------------------------------------------------------------------------------
let people = ["james", "mike","bill"] ;
for (let i = 0;i<people.length;i++){
	console.log("welcome  "+people[i]);
}
space();

//console.log(Math.random());
//console.log(Math.floor(5.6789));

function getRandomNumber() {
	let randomNumber = Math.floor(Math.random() * 10);
	return randomNumber;
}
let randomNumber = getRandomNumber();
while(randomNumber!==7){
	console.log(randomNumber + "n est pas égal a  7");
	randomNumber = getRandomNumber();
}

console.log(randomNumber + "est égal a 7 ");
space();

// instruction while jamais executé + do while "passe au moin 1x-------------------------------------------

let counter = 10;
while(counter<5){
	console.log(counter+"est < 5 ");
	counter++;
}
console.log("code non executé");
space();
let count = 1 ;

do {
	console.log(count);
	count++;
}
while (count < 1);
space();

// instruction "continue" -----------------------------------------------------------

let saying = "Qui restreint ses besoins sera d'autant plus libre.";
let vowels = ["a","e","i","o","u","y"];
result = vowels.indexOf(saying.charAt(4));
console.log(result);
let vowelsCounter = 0;
for(let i = 0;i< saying.length;i++){
	let character = saying.charAt(i);
	if (vowels.indexOf(character) == -1){
	console.log(character + " , nest pas une voyelle");
	continue;
	}
	vowelsCounter++;
}
console.log("nbr de voyells : "+vowelsCounter);
space();
// boucle infinie et break-------------------------------------------

while(true){
	randomNbr = getRandomNumber();
	if(randomNbr === 8){
		break;
	}
	console.log(randomNbr + " n'est pas le chiffre mystère");
	
}
console.log("8 est  le chiffre mystère");
space();

//acceder a tous les lien de la page----------------------------------------
let links = document.getElementsByTagName("a");console.log(links[5]);console.log(links.item(2));
space();

let jsDom1 = document.getElementById("jsDom1");
jsDom1.innerHTML = "javasript peut modifier le contenu du paragraphe"
let jsDom2 = document.getElementById("jsDom2");
jsDom2.style.color = "green";
jsDom2.style.fontSize = "130%";
jsDom2.style.fontWeight = "bold";

let jsDom3 = document.getElementById("jsDom3"); // attribue feuille de style
jsDom3.setAttribute("class", "js_dom3 ");

const jsdom4 = document.createElement("p");
let content4 = document.createTextNode("contenu temporaire");
jsdom4.appendChild(content4);
let main = document.getElementById("main");
main.appendChild(jsdom4);
jsdom4.innerHTML = "js peut cree des balises et du contenu"

// creer un bouton cacher/montrer

let toggleBtn = document.createElement("button");
main.appendChild(toggleBtn);
toggleBtn.setAttribute("class", "js_dom5")
toggleBtn.innerHTML = "cacher" ;
toggleBtn.onclick = function() {hideParagrahe("jsDom3")};

function hideParagrahe(id) {
	let para = document.getElementById(id);
	if (para.style.display === "none"){
		para.style.display = "block";
		toggleBtn.innerHTML = "cacher";

	}
	else{
		para.style.display = "none";
		toggleBtn.innerHTML = "montrer";
	}
}
